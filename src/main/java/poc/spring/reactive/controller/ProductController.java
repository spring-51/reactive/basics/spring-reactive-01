package poc.spring.reactive.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import poc.spring.reactive.model.Product;
import poc.spring.reactive.service.ProductService;
import reactor.core.publisher.Flux;

@RestController
public class ProductController {

    private final ProductService service;

    public ProductController(ProductService service) {
        this.service = service;
    }

    @GetMapping(value = "/products2", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Product> getAll2(){
        return service.getAll2();
    }
}
