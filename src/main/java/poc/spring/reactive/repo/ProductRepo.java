package poc.spring.reactive.repo;

import poc.spring.reactive.model.Product;
import reactor.core.publisher.Flux;

public interface ProductRepo{

    static Flux<Product> findAll2(){
        Flux<Product> response = Flux.just(
                new Product(10, "Beer"),
                new Product(11, "Book"),
                new Product(12, "Pen")
        );
        return response;
    }
}
