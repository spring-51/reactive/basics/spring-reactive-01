package poc.spring.reactive.service;

import org.springframework.stereotype.Service;
import poc.spring.reactive.model.Product;
import poc.spring.reactive.repo.ProductRepo;
import reactor.core.publisher.Flux;

import java.time.Duration;

@Service
public class ProductService {

    // private final ProductRepo repo;

    /*
    public ProductService(ProductRepo repo) {
        this.repo = repo;
    }

     */

    /*
    public Flux<Product> getAll(){
        Flux<Product> all = repo.findAll()
                // delaying n seconds with retrieval of each product,
                // assume as with every product we are performing some operations
                .delayElements(Duration.ofSeconds(5))
                ;
        return all;
    }

     */

    public Flux<Product> getAll2(){
        Flux<Product> all = ProductRepo.findAll2()
                // delaying n seconds with retrieval of each product,
                // assume as with every product we are performing some operations
                .delayElements(Duration.ofSeconds(5))
                ;
        return all;
    }
}
